@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'Add Category')
@section('content')
@section('headline', 'Add Category')
@section('content')

    <button class="btn btn-default"><a href="{{Route('category.index')}}">Category List</a> </button>

    <br><br>
    {!! Form::open(['url'=>'admin/category', 'method'=>'post',]) !!}

    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder'=>'Insert Category Name']) !!}
    <br>

    {!! Form::button('Add Category',['type'=>'submit', 'class'=>'btn btn-info']) !!}
    {!! Form::close() !!}


@endsection