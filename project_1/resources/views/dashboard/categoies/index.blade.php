@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'Category List')
@section('content')
@section('headline', 'Category List')
@section('content')
    @if(session()->has('status'))
        <p class="text-center text-success">{{session('status')}}</p>
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Serial</th>
            <th>Title</th>
            <th>Created By</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $sl=0;
        @endphp
@foreach($categories as $category)
        <tr>
            <th scope="row">{{++$sl}}</th>
            <td>{{$category->title}}</td>
            <td>Naim</td>
            <td>{{$category->created_at->diffForHumans()}}</td>
            <td>{{$category->updated_at->diffForHumans()}}</td>
            <td>
                <button class="btn btn-success"><a href="#">Show</a> </button>

                <button class="btn btn-info"><a href="#">Edit</a> </button>

                {!! Form::open(['url' => 'students/', 'method'=>'delete', 'style' => 'display:inline' ])!!}

                {!! Form::button('Delete', ['type'=>'submit', 'class'=> 'btn btn-danger' , 'onClick'=>"return confirm('Are you sure want to delete  ?')"]) !!}

                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach


        </tbody>
    </table>
    <button class="btn btn-outline-info"><a href="{{route('category.create')}}" >Create Category</a> </button>



@endsection
