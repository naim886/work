@extends('students.layouts.master')
@section('page_title' , 'Add New Student')
@section('breadcrumb', 'Add Student')
@section('content')

    <button class="btn btn-success"><a href="{{Route('students.index')}}">List View</a> </button>
    <h1 class="text-center">Update Student</h1>

    {!! Form::open(['url'=>'students/'.$student->id, 'method'=>'put',]) !!}

    {!! Form::text('fname', $student->fname, ['class' => 'form-control', 'placeholder'=>'Insert Your First Name']) !!}
    {!! Form::text('lname', $student->lname, ['class' => 'form-control', 'placeholder'=>'Insert Your Last Name']) !!}
    {!! Form::button('Update', ['type'=>'submit', 'class'=>'btn btn-info']) !!}
    {!! Form::close() !!}


@endsection