@extends('dashboard.layouts.master')
@section('page_title' , 'Post | Home')
@section('breadcrumb', 'Post List')
@section('content')
@section('headline', 'Post List')
@section('content')
@if(session()->has('status'))
   <p class="text-center text-success">{{session('status')}}</p>
@endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Serial</th>
            <th>Title</th>
            <th>Author</th>
            <th>Posted at</th>
            <th>Updated at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <th scope="row">SL</th>
            <td>Title</td>
            <td>Author</td>
            <td>Posted at</td>
            <td>Updated at</td>
            <td>
                <button class="btn btn-success"><a href="#">Show</a> </button>

                <button class="btn btn-info"><a href="#">Edit</a> </button>

                    {!! Form::open(['url' => 'students/', 'method'=>'delete', 'style' => 'display:inline' ])!!}

                    {!! Form::button('Delete', ['type'=>'submit', 'class'=> 'btn btn-danger' , 'onClick'=>"return confirm('Are you sure want to delete  ?')"]) !!}

                    {!! Form::close() !!}
            </td>
        </tr>


        </tbody>
    </table>
    <button class="btn btn-outline-info"><a href="#" >Create Post</a> </button>



    @endsection
