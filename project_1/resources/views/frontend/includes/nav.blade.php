<body>
<!-- header -->
<div class="header">
    <div class="container">
        <div class="logo">
            <a href="{{route('home.index')}}"><img src="{{asset('front/images/logo.png')}}" class="img-responsive" alt=""></a>
        </div>

        <div class="head-nav">
            <span class="menu"> </span>
            <ul class="cl-effect-1">
                <li {{ request()->route()->getName() === 'home.index' ? ' class=active' : '' }}><a href="{{route('home.index')}}">Home</a></li>
                <li {{ request()->route()->getName() === 'about' ? ' class=active' : '' }}><a href="{{route('about')}}">About Us</a></li>
                <li {{ request()->route()->getName() === 'blog.index' ? ' class=active' : '' }}><a href="{{route('blog.index')}}">Blog</a></li>
                <li {{ request()->route()->getName() === 'login' ? ' class=active' : '' }}><a href="{{route('login')}}">Login</a></li>
                <li {{ request()->route()->getName() === 'contact' ? ' class=active' : '' }}><a href="{{route('contact')}}">Contact</a></li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <!-- script-for-nav -->
        <script>
            $( "span.menu" ).click(function() {
                $( ".head-nav ul" ).slideToggle(300, function() {
                    // Animation complete.
                });
            });
        </script>
        <!-- script-for-nav -->



        <div class="clearfix"> </div>
    </div>
</div>