@extends('frontend.layouts.blog')
@section('page_title','Blog')
@section('content')




    <div class="container">
        <div class="content-top">
             <div class="single">


                <div class="single-top">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('blog.index')}}">blog</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Post</li>
                        </ol>
                    </nav>

                    <h2><a href="{{route('blog.show','1')}}">Printing and typesetting industry</a></h2>
                    <h6>Posted on, 12 July 2014 at 10.30am by <a href="#"> admin</a></h6>

                    <img src="{{asset('front/images/7.jpg')}}" class="img-responsive" alt="">

                    <p class="sin" style="text-align: justify">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tinci arted.
                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliq usit e
                        adipiscing elit, sed diam nonummy nibh euismod tinci. Donec sed odio dui. Duis mollis, est non cons
                        commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Amet, consectetuer  diam teri
                        adipiscing elit, sed diam nonummy nibh euismod tinci.</p>
                    <p class="sin" style="text-align: justify">Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue. Donec ullamco elitider
                        per nulla non metus auctor fringilla. Donec id elit non mi porta gravi da at eget metus. Fusce dap gravi
                        ibus, tellus ac  cursus commodo, Nulla vitae elit libero, a torto ediri
                        pharetra augue. Donec ullamco per nulla non metus auctor fringilla.</p>
                    <div class="artical-links">
                        <ul>
                            <li><small> </small><span>june 14, 2013</span></li>
                            <li><a href="#"><small class="admin"> </small><span>admin</span></a></li>
                            <li><a href="#"><small class="no"> </small><span>No comments</span></a></li>
                            <li><a href="#"><small class="posts"> </small><span>View posts</span></a></li>

                        </ul>
                    </div>
                    <div class="respon">
                        <h2>Responses so far</h2>
                        <div class="strator">
                            <h5>ADMINISTRATOR</h5>
                            <p>feb 20th, 2015 at 9:41 pm</p>
                            <div class="strator-left">
                                <img src="images/co.png" class="img-responsive" alt="">
                            </div>
                            <div class="strator-right">
                                <p class="sin">Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra aug
                                    metus auctor fringilla. Donec id elit non mi porta  da at eget me  us, tellus ac
                                    ortor mauris ntum nibh, ut fermentum massa risus. Sed posuere consectetur
                                    Nulla vitae elit liber. Sed posuere consectetur est at lobortis.</p>
                            </div>
                            <div class="clearfix"></div>

                        </div>

                        <div class="comment">
                            <h2>Leave a comment</h2>
                            <form method="post" action="">
                                <input type="text" class="textbox" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                                <input type="text" class="textbox" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                                <input type="text" class="textbox" value="Website" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Website';}">
                                <textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                                <div class="smt1">
                                    <input type="submit" value="add a comment">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


@endsection