@extends('frontend.layouts.blog')
@section('page_title','Blog')
@section('content')

    <div class="container">
        <div class="blog">

            <div class="blog-content">
                <div class="blog-content-left">
                    <div class="blog-articals">



                        {{--post start from here--}}

                        <div class="blog-artical">
                            <div class="blog-artical-info">
                                <div class="blog-artical-info-img">
                                    <a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/7.jpg')}}" title="post-name"></a>
                                </div>
                                <div class="blog-artical-info-head">
                                    <h2><a href="{{route('blog.show','1')}}">Printing and typesetting industry</a></h2>
                                    <h6>Posted on, 12 July 2014 at 10.30am by <a href="#"> admin</a></h6>

                                </div>
                                <div class="blog-artical-info-text">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy has been the industry's standard dummy has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<a href="#">[...]</a></p>
                                </div>
                                <div class="artical-links">
                                    <ul>
                                        <li><small> </small><span>june 14, 2013</span></li>
                                        <li><a href="#"><small class="admin"> </small><span>admin</span></a></li>
                                        <li><a href="#"><small class="no"> </small><span>No comments</span></a></li>
                                        <li><a href="#"><small class="posts"> </small><span>View posts</span></a></li>
                                        <li><a href="#"><small class="link"> </small><span>permalink</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>



                    {{--Post ends here--}}





                        <div class="blog-artical">

                            <div class="blog-artical-info">
                                <div class="blog-artical-info-img">
                                    <a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/8.jpg')}}" title="post-name"></a>

                                </div>
                                <div class="blog-artical-info-head">
                                    <h2><a href="{{route('blog.show','1')}}">Simply dummy text of the</a></h2>
                                    <h6>Posted on, 12 July 2014 at 10.30am by <a href="#"> admin</a></h6>

                                </div>
                                <div class="blog-artical-info-text">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy has been the industry's standard dummy has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<a href="#">[...]</a></p>
                                </div>
                                <div class="artical-links">
                                    <ul>
                                        <li><small> </small><span>june 14, 2013</span></li>
                                        <li><a href="#"><small class="admin"> </small><span>admin</span></a></li>
                                        <li><a href="#"><small class="no"> </small><span>No comments</span></a></li>
                                        <li><a href="#"><small class="posts"> </small><span>View posts</span></a></li>
                                        <li><a href="#"><small class="link"> </small><span>permalink</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="blog-artical">

                            <div class="blog-artical-info">
                                <div class="blog-artical-info-img">
                                    <a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/.jpg')}}" title="post-name"></a>
                                </div>
                                <div class="blog-artical-info-head">
                                    <h2><a href="{{route('blog.show','1')}}">Lorem Ipsum has been the</a></h2>
                                    <h6>Posted on, 12 July 2014 at 10.30am by <a href="#"> admin</a></h6>

                                </div>
                                <div class="blog-artical-info-text">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy has been the industry's standard dummy has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<a href="#">[...]</a></p>
                                </div>
                                <div class="artical-links">
                                    <ul>
                                        <li><small> </small><span>june 14, 2013</span></li>
                                        <li><a href="#"><small class="admin"> </small><span>admin</span></a></li>
                                        <li><a href="#"><small class="no"> </small><span>No comments</span></a></li>
                                        <li><a href="#"><small class="posts"> </small><span>View posts</span></a></li>
                                        <li><a href="#"><small class="link"> </small><span>permalink</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <!--start-blog-pagenate-->
                    <nav>
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!--//End-blog-pagenate-->


</div>

@endsection