<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});



Route::get('/', 'frontendController@index')->name('home.index');
Route::get('/about', 'frontendController@about')->name('about');
//Route::get('/blog', 'frontendController@index')->name('blog');
//Route::get('/login', 'frontendController@index')->name('login');
Route::get('/contact', 'frontendController@contact')->name('contact');
Route::get('/show', 'frontendController@index')->name('show');


Route::get('/blog', 'blogController@index')->name('blog.index');
Route::get('blog/show/{id}', 'blogController@show')->name('blog.show');

Auth::routes();


Route::group(['middleware'=>'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('admin/', 'postsController@index')->name('post.index');
    Route::get('admin/create', 'postsController@index')->name('post.create');

    Route::get('admin/category', 'CategoriesController@index')->name('category.index');
    Route::get('admin/category/create', 'CategoriesController@create')->name('category.create');
    Route::post('admin/category', 'CategoriesController@store')->name('category.store');
    Route::get('admin/category/{id}', 'CategoriesController@show')->name('category.show');
});






